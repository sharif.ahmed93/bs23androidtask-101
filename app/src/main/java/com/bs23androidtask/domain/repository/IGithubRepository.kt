package com.bs23androidtask.domain.repository

import com.bs23androidtask.data.source.remote.Resource
import com.bs23androidtask.data.source.remote.model.response.GithubRepoResponse
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.data.source.remote.model.response.Owner
import kotlinx.coroutines.flow.Flow

interface IGithubRepository {

    suspend fun getRepositories(
        queryData: String,
        page: Int,
        pageSize: Int,
        sort: String? = null
    ): Flow<Resource<GithubRepoResponse>>

    suspend fun getRepositoriesNetworkBound(
        queryData: String,
        page: Int,
        pageSize: Int,
        sort: String? = null
    ): Flow<Resource<List<Item>>>
}
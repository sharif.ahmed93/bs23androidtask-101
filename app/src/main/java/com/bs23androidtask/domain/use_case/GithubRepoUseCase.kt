package com.bs23androidtask.domain.use_case

import com.bs23androidtask.data.source.remote.Resource
import com.bs23androidtask.data.source.remote.model.response.GithubRepoResponse
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.domain.repository.IGithubRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

data class GithubRepoUseCase(
    val getRepositoryUseCase: GetRepositoryUseCase,
    val getRepositoryUseCaseNetworkBound: GetRepositoryUseCaseNetworkBound,
    val getRepositoryUseCaseBySort: GetRepositoryUseCaseBySort,
    val saveToLocalUseCase: SaveToLocalUseCase,
)

class GetRepositoryUseCaseNetworkBound @Inject constructor(
    private val repository: IGithubRepository
) {
    suspend operator fun invoke(
        query: String,
        page: Int,
        pageSize: Int = 10,
        sort: String? = null
    ): Flow<Resource<List<Item>>> {
        return repository.getRepositoriesNetworkBound(query, page, pageSize, sort)
    }
}

class GetRepositoryUseCase @Inject constructor(
    private val repository: IGithubRepository
) {
    suspend operator fun invoke(
        query: String,
        page: Int,
        pageSize: Int = 10
    ): Flow<Resource<GithubRepoResponse>> {
        return repository.getRepositories(query, page, pageSize)
    }
}

class GetRepositoryUseCaseBySort @Inject constructor(
    private val repository: IGithubRepository
) {
    suspend operator fun invoke(
        query: String,
        page: Int,
        pageSize: Int = 10,
        sort: String
    ): Flow<Resource<GithubRepoResponse>> {
        return repository.getRepositories(query, page, pageSize, sort)
    }
}

class SaveToLocalUseCase @Inject constructor(
    private val repository: IGithubRepository
) {
    suspend operator fun invoke() {

    }
}
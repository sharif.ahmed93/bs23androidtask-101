package com.bs23androidtask.presentation.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bs23androidtask.R
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.databinding.DetailsActivityBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailsActivity : AppCompatActivity() {
    private var binding: DetailsActivityBinding? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        setUpData()
    }

    private fun setUpViews() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
    }

    private fun setUpData() {
        val extras = intent.extras
        val item: Item?
        if (extras != null) {
            item = extras.getSerializable("ITEM") as Item?
            binding?.tvName?.text = item?.name
            binding?.tvDescription?.text = item?.description
            binding?.tvDate?.text = item?.updatedAt
            binding?.ivAvatar?.let {
                Glide.with(this).load(item?.owner?.avatarUrl).into(it)
            }
        }
    }

}
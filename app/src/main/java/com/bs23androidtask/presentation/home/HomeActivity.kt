package com.bs23androidtask.presentation.home

import android.app.Dialog
import android.content.ClipData
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.bs23androidtask.AppExecutors
import com.bs23androidtask.R
import com.bs23androidtask.data.source.local.preferences.AppPreferences
import com.bs23androidtask.data.source.remote.Resource
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.databinding.ActivityHomeBinding
import com.bs23androidtask.hideLoadingDialog
import com.bs23androidtask.presentation.details.DetailsActivity
import com.bs23androidtask.showLoadingDialog
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    private val TAG: String? = HomeActivity::class.simpleName

    private var progressDialog: Dialog? = null
    private val viewModel by viewModels<HomeViewModel>()
    private var binding: ActivityHomeBinding? = null

    private lateinit var repoListAdapter: RepoListAdapter
    private var repoList: ArrayList<Item> = ArrayList()

    @Inject
    lateinit var appPreferences: AppPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpViews()
        observeAPICall()
        setupObservers()
        setAdapter()
    }

    override fun onResume() {
        super.onResume()
        observeAPICall()
    }

    private fun setAdapter() {
        repoListAdapter = RepoListAdapter(repoList) {
            openDetailsPage(it)
        }
        binding?.rcvList?.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding?.rcvList?.hasFixedSize()
        binding?.rcvList?.adapter = repoListAdapter
    }

    private fun setUpViews() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        appPreferences.sortBy?.let {
            when (it) {
                "stars" -> {
                    binding?.rbStars?.isChecked = true
                }
                "updated" -> {
                    binding?.rbUpdated?.isChecked = true
                }
            }
        }
        binding?.radioGroup?.setOnCheckedChangeListener { radioGroup, checkedId ->
            when (checkedId) {
                R.id.rbStars -> {
                    appPreferences.sortBy = "stars"
                    //viewModel.getGithubRepoUseSort(appPreferences.sortBy!!)
                    viewModel.getGithubRepoNetworkBound(appPreferences.sortBy!!)
                }
                R.id.rbUpdated -> {
                    appPreferences.sortBy = "updated"
                    //viewModel.getGithubRepoUseSort(appPreferences.sortBy!!)
                    viewModel.getGithubRepoNetworkBound(appPreferences.sortBy!!)
                }
            }

        }
    }

    private fun observeAPICall() {
        if (TextUtils.isEmpty(appPreferences.sortBy) || appPreferences.sortBy == null) {
            //viewModel.getGithubRepo()
            viewModel.getGithubRepoNetworkBound()
        } else {
            //viewModel.getGithubRepoUseSort(appPreferences.sortBy!!)
            viewModel.getGithubRepoNetworkBound(appPreferences.sortBy!!)
        }
    }

    private fun setupObservers() {
        lifecycleScope.launchWhenResumed {
            viewModel.githubRepoResponse.collect {
                executeNetworkResults(
                    resourceData = it,
                    doOnSuccess = { result ->
                        repoList.clear()
                        result.items?.let { it1 -> repoList.addAll(it1) }
                        repoListAdapter.notifyDataSetChanged()
                    },
                    doOnError = { errorMsg, data ->
                        Toast.makeText(applicationContext, errorMsg, Toast.LENGTH_SHORT).show()
                    }
                )
            }
        }

        lifecycleScope.launchWhenResumed {
            viewModel.githubRepoResponseNetworkBound.collect {
                executeNetworkResults(
                    resourceData = it,
                    doOnSuccess = { result ->
                        repoList.clear()
                        result.let { it1 -> repoList.addAll(it1) }
                        repoListAdapter.notifyDataSetChanged()
                    },
                    doOnError = { errorMsg, data ->
                        Toast.makeText(applicationContext, errorMsg, Toast.LENGTH_SHORT).show()
                    }
                )
            }
        }
    }

    private fun showLoading(hint: String?) {
        hideLoading()
        progressDialog = showLoadingDialog(this, hint)
    }

    private fun hideLoading() = hideLoadingDialog(progressDialog, this)

    private fun openDetailsPage(intentData: Item) = run {
        val bundle = Bundle()
        bundle.putSerializable("ITEM", intentData)
        val intent = Intent(this, DetailsActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun <T> executeNetworkResults(
        resourceData: Resource<T>,
        doOnLoading: (() -> Unit)? = null,
        doOnSuccess: (data: T) -> Unit,
        doOnError: ((errorMessage: String?, data: T?) -> Unit)? = null
    ) {
        when (resourceData) {
            is Resource.Loading -> {
                if (doOnLoading == null) {
                    try {
                        showLoading("Please Wait..")
                    } catch (e: Exception) {
                        showLoading("Please Wait..")
                    }
                } else
                    doOnLoading.invoke()
            }
            is Resource.Success -> {
                if (doOnLoading == null)
                    hideLoading()
                doOnSuccess.invoke(resourceData.data)
            }
            is Resource.Error -> {
                doOnError?.invoke(resourceData.errorMessage, resourceData.data)
                if (doOnLoading == null)
                    hideLoading()
            }
            else -> {}
        }
    }

}

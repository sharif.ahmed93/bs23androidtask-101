package com.bs23androidtask.presentation.home

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bs23androidtask.data.source.local.preferences.AppPreferences
import com.bs23androidtask.data.source.remote.Resource
import com.bs23androidtask.data.source.remote.model.response.GithubRepoResponse
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.domain.use_case.GithubRepoUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val useCase: GithubRepoUseCase,
    private val appPreferences: AppPreferences
) : ViewModel() {

    private val _githubRepoResponse =
        MutableStateFlow<Resource<GithubRepoResponse>>(Resource.Default)
    val githubRepoResponse = _githubRepoResponse

    private val _githubRepoResponseNetworkBound =
        MutableStateFlow<Resource<List<Item>>>(Resource.Default)
    val githubRepoResponseNetworkBound = _githubRepoResponseNetworkBound

    init {
        //getGithubRepo()
        getGithubRepoNetworkBound()
    }

    fun getGithubRepo() {
        viewModelScope.launch {
            useCase.getRepositoryUseCase(
                "Android",
                1,
                100
            ).onEach { result ->
                _githubRepoResponse.value = result
            }.launchIn(viewModelScope)
        }
    }

    fun getGithubRepoNetworkBound(sortBy: String? = null) {
        viewModelScope.launch {
            useCase.getRepositoryUseCaseNetworkBound(
                "Android",
                1,
                100,
                sortBy
            ).onEach { result ->
                _githubRepoResponseNetworkBound.value = result
            }.launchIn(viewModelScope)
        }
    }

    fun getGithubRepoUseSort(sortBy: String) {
        Log.d("TAG", "getGithubRepoUseSort: $sortBy")
        viewModelScope.launch {
            useCase.getRepositoryUseCaseBySort(
                query = "Android",
                page = 1,
                pageSize = 100,
                sort = sortBy
            ).onEach { result ->
                _githubRepoResponse.value = result
            }.launchIn(viewModelScope)
        }
    }
}
package com.bs23androidtask.presentation.home

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bs23androidtask.R
import com.bs23androidtask.data.source.remote.model.response.Item
import com.bs23androidtask.databinding.RepoItemBinding
import java.util.*

class RepoListAdapter(
    private val arrayList: ArrayList<Item>,
    private val onItemClickListener: (Item)->Unit
) : RecyclerView.Adapter<RepoListAdapter.RepoListViewHolder?>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepoListViewHolder {
        val repoItemBinding: RepoItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_rcv_list,
            parent,
            false
        )
        return RepoListViewHolder(repoItemBinding)
    }

    override fun onBindViewHolder(holder: RepoListViewHolder, position: Int) {
        holder.listLayoutBinding.name.text = arrayList[position].fullName
        holder.listLayoutBinding.desc.text = arrayList[position].description
        holder.listLayoutBinding.updatedAt.text = arrayList[position].updatedAt
        holder.listLayoutBinding.stars.text = arrayList[position].stargazersCount.toString()
        holder.listLayoutBinding.root.setOnClickListener {
            onItemClickListener.invoke(arrayList[position])
        }
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    inner class RepoListViewHolder(binding: RepoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val listLayoutBinding: RepoItemBinding = binding

    }
}

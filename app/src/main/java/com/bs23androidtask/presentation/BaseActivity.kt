package com.bs23androidtask.presentation

import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.bs23androidtask.hideLoadingDialog
import com.bs23androidtask.showLoadingDialog

abstract class BaseActivity<VB : ViewDataBinding, V : ViewModel> : AppCompatActivity() {

    private var progressDialog: Dialog? = null

    private var _binding: VB? = null

    open val viewDataBinding get() = _binding!!

    abstract val bindingVariable: Int

    @get:LayoutRes
    abstract val layoutId: Int

    abstract val viewModel: V

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        _binding = DataBindingUtil.setContentView(this, layoutId)
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.setVariable(bindingVariable, viewModel)
        viewDataBinding.executePendingBindings()
        setUpViews()
        observeAPICall()
        setupObservers()
    }


    open fun setUpViews() {}

    open fun observeAPICall() {}

    open fun setupObservers() {}

    fun showLoading(hint: String?) {
        hideLoading()
        progressDialog = showLoadingDialog(this, hint)
    }

    fun hideLoading() = hideLoadingDialog(progressDialog, this)
}
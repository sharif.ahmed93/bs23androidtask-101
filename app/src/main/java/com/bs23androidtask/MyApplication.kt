package com.bs23androidtask

import android.app.Application
import android.content.Context
import dagger.hilt.InstallIn
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MyApplication: Application() {
    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
    }
}
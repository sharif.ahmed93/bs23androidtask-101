package com.bs23androidtask.data.source.remote.model.response

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

data class GithubRepoResponse(
    @SerializedName("total_count")
    @Expose
    var totalCount: Int? = null,

    @SerializedName("incomplete_results")
    @Expose
    var incompleteResults: Boolean? = null,


    @SerializedName("items")
    @Expose
    var items: List<Item>? = null
)

@Entity
data class Item(
    @PrimaryKey
    @SerializedName("id")
    @Expose
    var id: Int,

    @SerializedName("name")
    @Expose
    var name: String? = null,

    @SerializedName("full_name")
    @Expose
    var fullName: String? = null,

    @TypeConverters
    @SerializedName("owner")
    @Expose
    var owner: Owner? = null,

    @SerializedName("description")
    @Expose
    var description: String? = null,

    @SerializedName("created_at")
    @Expose
    var createdAt: String? = null,

    @SerializedName("updated_at")
    @Expose
    var updatedAt: String? = null,

    @SerializedName("pushed_at")
    @Expose
    var pushedAt: String? = null,

    @SerializedName("size")
    @Expose
    var size: Int? = null,

    @SerializedName("stargazers_count")
    @Expose
    var stargazersCount: Int? = null,

    @SerializedName("has_pages")
    @Expose
    var hasPages: Boolean? = null,

    @SerializedName("topics")
    @Expose
    var topics: List<String>? = null

) : Serializable

data class Owner (
    @SerializedName("login")
    @Expose
    var login: String? = null,

    @SerializedName("id")
    @Expose
    var id: Int? = null,

    @SerializedName("avatar_url")
    @Expose
    var avatarUrl: String? = null
): Serializable
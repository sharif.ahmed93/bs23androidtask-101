package com.bs23androidtask.data.source.remote

import com.bs23androidtask.data.source.remote.model.response.GithubRepoResponse
import com.bs23androidtask.data.source.remote.model.response.Owner
import com.bs23androidtask.data.source.remote.network.ApiServices
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class RemoteDataSource @Inject constructor(
    private val apiService: ApiServices
) : BaseRemoteDataSource() {

    suspend fun getRepositories(
        queryData: String,
        page: Int,
        pageSize: Int,
        sort:String?
    ): Flow<ApiResponse<GithubRepoResponse>> =  flow {
        try {
            emit(safeApiCall { apiService.getRepositories(queryData,page,pageSize,sort) })
        } catch (e: Exception) {

        }
    }.flowOn(Dispatchers.IO)

}
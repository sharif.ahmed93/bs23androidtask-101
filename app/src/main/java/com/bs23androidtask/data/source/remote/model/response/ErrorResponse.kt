package com.bs23androidtask.data.source.remote.model.response

data class ErrorResponse(
    val detail: String,
    val instance: Any,
    val status: Int,
    val title: String,
    val type: String
)
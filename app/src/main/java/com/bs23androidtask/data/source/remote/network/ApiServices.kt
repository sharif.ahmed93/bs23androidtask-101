package com.bs23androidtask.data.source.remote.network

import com.bs23androidtask.data.source.remote.constants.ApiEndPoint
import com.bs23androidtask.data.source.remote.model.response.GithubRepoResponse
import com.bs23androidtask.data.source.remote.model.response.Owner
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {
    @GET(ApiEndPoint.ENDPOINT_SEARCH_REPOSITORIES)
    suspend fun getRepositories(
        @Query("q") searchData: String,
        @Query("page") page: Int = 1,
        @Query("per_page") pageSize: Int = 10,
        @Query("sort") useSort: String? = null,
    ): GithubRepoResponse
}
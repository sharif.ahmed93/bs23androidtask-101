package com.bs23androidtask.data.source.remote.constants

object ApiEndPoint {
    const val ENDPOINT_SEARCH_REPOSITORIES = "search/repositories"
}
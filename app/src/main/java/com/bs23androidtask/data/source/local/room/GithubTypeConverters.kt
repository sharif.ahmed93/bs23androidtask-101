/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bs23androidtask.data.source.local.room

import androidx.room.TypeConverter
import com.bs23androidtask.data.source.remote.model.response.Owner
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONObject

 class GithubTypeConverters {
    @TypeConverter
    fun stringToIntList(data: String?): List<Int>? {
        return data?.let {
            it.split(",").map {
                try {
                    it.toInt()
                } catch (ex: NumberFormatException) {
                    null
                }
            }
        }?.filterNotNull()
    }

    @TypeConverter
    fun intListToString(ints: List<Int>?): String? {
        return ints?.joinToString(",")
    }

    @TypeConverter
    fun fromListStringObject(list: List<String>?): String? {
        val type = object : TypeToken<List<String>?>() {}.type
        return Gson().toJson(list, type)
    }

    @TypeConverter
    fun toListStringObject(list: String?): List<String>? {
        val type = object : TypeToken<List<String>?>() {}.type
        return Gson().fromJson(list, type)
    }

    @TypeConverter
    fun fromOwner(owner: Owner): String {
        return JSONObject().apply {
            put("id", owner.id)
            put("login", owner.login)
            put("avatarUrl", owner.avatarUrl)
        }.toString()
    }

    @TypeConverter
    fun toOwner(owner: String): Owner {
        val json = JSONObject(owner)
        return Owner(
            json.getString("login"),
            json.getInt("id"),
            json.getString("avatarUrl"),
        )
    }
}

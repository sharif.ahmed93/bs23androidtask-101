package com.bs23androidtask.data.source.remote.model.response

data class BaseResponse<T>(
    val result: T,
    val detail: String
)

/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bs23androidtask.data.source.local.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.bs23androidtask.data.source.remote.model.response.Item
import kotlinx.coroutines.flow.Flow

/**
 * Interface for database access on Repo related operations.
 */
@Dao
interface RepoDao {
    @Query("SELECT * FROM item")
    fun getRepoData(): Flow<List<Item>>

    @Query("SELECT * FROM item ORDER BY stargazersCount DESC")
    fun getRepoDataByStars(): Flow<List<Item>>

    @Query("SELECT * FROM item ORDER BY updatedAt DESC")
    fun getRepoDataByUpdated(): Flow<List<Item>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRepo(items: List<Item>)
}

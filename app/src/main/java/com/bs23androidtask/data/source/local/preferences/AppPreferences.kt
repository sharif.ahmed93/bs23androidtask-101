package com.bs23androidtask.data.source.local.preferences

import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class AppPreferences @Inject constructor(private val context: Context) {

    companion object {
        private const val APP_PREFERENCES_NAME = "APP-NAME-Cache"
        private const val SESSION_PREFERENCES_NAME = "APP-NAME-UserCache"
        private const val MODE = Context.MODE_PRIVATE
        private val FIRST_TIME = Pair("FIRST_TIME", true)
        private val SORT_BY = Pair("SORT_BY", null)
    }

    private val appPreferences: SharedPreferences =
        context.getSharedPreferences(APP_PREFERENCES_NAME, MODE)
    private val sessionPreferences: SharedPreferences =
        context.getSharedPreferences(SESSION_PREFERENCES_NAME, MODE)

    /**
     * SharedPreferences extension function, so we won't need to call edit() and apply()
     * ourselves on every SharedPreferences operation.
     */
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }

    var isFirstTime: Boolean
        get() {
            return appPreferences.getBoolean(FIRST_TIME.first, FIRST_TIME.second)
        }
        set(value) = appPreferences.edit {
            it.putBoolean(FIRST_TIME.first, value)
        }

    var sortBy: String?
        get() {
            return appPreferences.getString(SORT_BY.first, SORT_BY.second)
        }
        set(value) = appPreferences.edit {
            it.putString(SORT_BY.first, value)
        }

    fun clearPreferences() {
        sessionPreferences.edit {
            it.clear().apply()
        }
    }
}
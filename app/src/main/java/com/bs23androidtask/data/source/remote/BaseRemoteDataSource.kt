package com.bs23androidtask.data.source.remote

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.bs23androidtask.data.source.remote.model.response.BaseResponse
import com.bs23androidtask.data.source.remote.model.response.ErrorResponse
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.net.ConnectException
import java.net.UnknownHostException
import javax.inject.Inject

open class BaseRemoteDataSource @Inject constructor() {

    suspend fun <T> safeApiCall(apiCall: suspend () -> T): ApiResponse<T> {
        try {
            val apiResponse = apiCall()

            when (apiResponse) {
                null -> {
                    return ApiResponse.Failure(FailureStatus.EMPTY)
                }
                is List<*> -> {
                    return if ((apiResponse as List<*>).isNotEmpty()) {
                        ApiResponse.Success(apiResponse)
                    } else {
                        ApiResponse.Failure(FailureStatus.EMPTY)
                    }
                }
                is Boolean -> {
                    return if (apiResponse as Boolean) {
                        ApiResponse.Success(apiResponse)
                    } else {
                        ApiResponse.Failure(FailureStatus.API_FAIL, 200, "apiResponse.detail")
                    }
                }
                else -> {
                    return ApiResponse.Success(apiResponse)
                }
            }
        } catch (throwable: Throwable) {
            when (throwable) {
                is HttpException -> {
                    when {
                        throwable.code() == 422 -> {
                            val jObjError =
                                JSONObject(throwable.response()!!.errorBody()!!.string())
                            val apiResponse = jObjError.toString()
                            val response = Gson().fromJson(apiResponse, BaseResponse::class.java)

                            return ApiResponse.Failure(
                                FailureStatus.API_FAIL,
                                throwable.code(),
                                response.detail
                            )
                        }
                        throwable.code() == 401 -> {
                            val errorResponse = Gson().fromJson(
                                throwable.response()?.errorBody()!!.charStream().readText(),
                                ErrorResponse::class.java
                            )

                            return ApiResponse.Failure(
                                FailureStatus.API_FAIL,
                                throwable.code(),
                                errorResponse.detail
                            )
                        }
                        else -> {
                            return if (throwable.response()?.errorBody()!!.charStream().readText()
                                    .isNullOrEmpty()
                            ) {
                                ApiResponse.Failure(FailureStatus.API_FAIL, throwable.code())
                            } else {
                                try {
                                    val errorResponse = Gson().fromJson(
                                        throwable.response()?.errorBody()!!.charStream().readText(),
                                        ErrorResponse::class.java
                                    )

                                    ApiResponse.Failure(
                                        FailureStatus.API_FAIL,
                                        throwable.code(),
                                        errorResponse.detail
                                    )
                                } catch (ex: JsonSyntaxException) {
                                    ApiResponse.Failure(FailureStatus.API_FAIL, throwable.code())
                                }
                            }
                        }
                    }
                }

                is UnknownHostException -> {
                    return ApiResponse.Failure(FailureStatus.NO_INTERNET)
                }

                is ConnectException -> {
                    return ApiResponse.Failure(FailureStatus.NO_INTERNET)
                }

                else -> {
                    return ApiResponse.Failure(FailureStatus.OTHER,null,throwable.message)
                }
            }
        }
    }
}

sealed class ApiResponse<out R> {

    data class Success<out T>(val data: T) : ApiResponse<T>()

    data class Error<T>(val errorMessage: String,val data: T? = null) : ApiResponse<T>()

    object Empty : ApiResponse<Nothing>()

    class Failure(val failureStatus: FailureStatus, val code: Int? = null, val message: String? = null) : ApiResponse<Nothing>()
}

sealed class Resource<out T> {

    class Success<out T>(val data: T) : Resource<T>()

    data class Error<T>(val errorMessage: String?,val data: T? = null) : Resource<T>()

    object Loading : Resource<Nothing>()

    //class Loading<T>(val data: T? = null) : Resource<T>()

    object Default : Resource<Nothing>()
}

enum class FailureStatus {
    EMPTY,
    API_FAIL,
    NO_INTERNET,
    OTHER
}
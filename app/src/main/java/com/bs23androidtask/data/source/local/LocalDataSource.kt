package com.bs23androidtask.data.source.local

import androidx.sqlite.db.SimpleSQLiteQuery
import com.bs23androidtask.data.source.local.room.RepoDao
import com.bs23androidtask.data.source.remote.model.response.Item
import kotlinx.coroutines.flow.Flow

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocalDataSource @Inject constructor(
    private val repoDao: RepoDao,
) {

    suspend fun insertRepoData(items: List<Item>) = repoDao.insertRepo(items)

    fun getRepoData(): Flow<List<Item>> = repoDao.getRepoData()

    fun getRepoDataByStars(): Flow<List<Item>> = repoDao.getRepoDataByStars()

    fun getRepoDataByUpdated(): Flow<List<Item>> = repoDao.getRepoDataByUpdated()

}
package com.bs23androidtask.data

import com.bs23androidtask.data.source.local.LocalDataSource
import com.bs23androidtask.data.source.remote.ApiResponse
import com.bs23androidtask.data.source.remote.Resource
import com.bs23androidtask.data.source.remote.RemoteDataSource
import com.bs23androidtask.data.source.remote.model.response.*
import com.bs23androidtask.domain.repository.IGithubRepository
import com.bs23androidtask.util.RateLimiter
import kotlinx.coroutines.flow.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class GithubDataRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource,
    private val localDataSource: LocalDataSource,
) : IGithubRepository {

    private val repoListRateLimit = RateLimiter<String>(30, TimeUnit.MINUTES)

    override suspend fun getRepositories(
        queryData: String,
        page: Int,
        pageSize: Int,
        sort: String?
    ): Flow<Resource<GithubRepoResponse>> {
        return flow {
            emit(Resource.Loading)
            emitAll(
                remoteDataSource.getRepositories(queryData, page, pageSize, sort)
                    .map {
                        when (it) {
                            is ApiResponse.Success -> {
                                Resource.Success(it.data)
                            }
                            is ApiResponse.Error -> {
                                Resource.Error(it.errorMessage, it.data)
                            }
                            is ApiResponse.Failure -> {
                                Resource.Error(it.message, null)
                            }
                            else -> {
                                Resource.Error("Something is wrong!", null)
                            }
                        }
                    }
            )
        }
    }

    override suspend fun getRepositoriesNetworkBound(
        queryData: String,
        page: Int,
        pageSize: Int,
        sort: String?
    ): Flow<Resource<List<Item>>> {
        /*return object : NetworkBoundResource<GithubRepoResponse, GithubRepoResponse>() {
            override fun loadFromDB(): Flow<GithubRepoResponse> {
                return flow {
                    localDataSource.getRepoData()
                }
            }

            override fun shouldFetch(data: GithubRepoResponse?): Boolean =
                repoListRateLimit.shouldFetch(queryData)

            override suspend fun createCall(): Flow<ApiResponse<GithubRepoResponse>> {
                return flow { remoteDataSource.getSearchRepositories(queryData, page, pageSize,sort) }
            }

            override suspend fun saveCallResult(data: GithubRepoResponse) {
                data.items?.let { localDataSource.insertRepoData(it) }
            }

            override fun onFetchFailed() {
                repoListRateLimit.reset(queryData)
            }
        }.asFlow()*/
        return object : NetworkBoundResource<List<Item>, GithubRepoResponse>() {
            override fun loadFromDB(): Flow<List<Item>> {
                return when (sort) {
                    "stars" -> localDataSource.getRepoDataByStars()
                    "updated" -> localDataSource.getRepoDataByUpdated()
                    else -> localDataSource.getRepoData()
                }
            }

            override fun shouldFetch(data: List<Item>?): Boolean {
                return repoListRateLimit.shouldFetch(queryData)
            }

            override suspend fun createCall(): Flow<ApiResponse<GithubRepoResponse>> =
                remoteDataSource.getRepositories(
                    queryData,
                    page,
                    pageSize,
                    sort
                )

            override suspend fun saveCallResult(data: GithubRepoResponse) {
                data.items?.let { localDataSource.insertRepoData(it) }
            }

            override fun onFetchFailed() {
                repoListRateLimit.reset(queryData)
            }

        }.asFlow()
    }

}
package com.bs23androidtask.di.module


import com.bs23androidtask.domain.repository.IGithubRepository
import com.bs23androidtask.domain.use_case.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Provides
    @Singleton
    fun provideSearchUseCases(repository: IGithubRepository): GithubRepoUseCase {
        return GithubRepoUseCase(
            saveToLocalUseCase = SaveToLocalUseCase(repository),
            getRepositoryUseCaseNetworkBound = GetRepositoryUseCaseNetworkBound(repository),
            getRepositoryUseCase = GetRepositoryUseCase(repository),
            getRepositoryUseCaseBySort = GetRepositoryUseCaseBySort(repository),
        )
    }

}
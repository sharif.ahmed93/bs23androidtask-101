package com.bs23androidtask.di.module

import com.bs23androidtask.data.source.remote.RemoteDataSource
import com.bs23androidtask.data.GithubDataRepository
import com.bs23androidtask.data.source.local.LocalDataSource
import com.bs23androidtask.domain.repository.IGithubRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideSearchRepository(
        remoteDataSource: RemoteDataSource,
        localDataSource: LocalDataSource
    ): IGithubRepository = GithubDataRepository(remoteDataSource,localDataSource)

}